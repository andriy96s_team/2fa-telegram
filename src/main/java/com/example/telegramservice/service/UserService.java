package com.example.telegramservice.service;

import com.example.telegramservice.bot.TwoFactorAuthenticationBot;
import com.example.telegramservice.entity.User;
import com.example.telegramservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final TwoFactorAuthenticationBot bot;

    @Autowired
    public UserService(UserRepository userRepository, TwoFactorAuthenticationBot bot) {
        this.userRepository = userRepository;
        this.bot = bot;
    }

    public String sendCode(String tgId) {
        User user = userRepository.findByTgId(tgId);
        String code = randomNumeric(8);
        boolean isOk = bot.sendCode(user.getChatId(), code);
        if (isOk) {
            user.setCode(code);
            userRepository.save(user);
        }
        return code;
    }

    public boolean verifyCode(String tgId, String code) {
        User user = userRepository.findByTgId(tgId);
        return code.equals(user.getCode());
    }

    private String randomNumeric(int length) {
        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {

            int number = (int) Math.round(Math.random() * 9);
            builder.append(number);
        }
        return builder.toString();
    }

    public boolean isUserExists(long chatId) {
        return userRepository.findById(chatId).isPresent();
    }

    public void saveNewUser(long chatId, UUID tgId) {
        User user = new User();
        user.setChatId(chatId);
        user.setTgId(tgId.toString());
        userRepository.save(user);
    }

    public String getTgIdForExistingUser(long chatId) {
        return userRepository.findById(chatId).orElse(new User()).getTgId();
    }
}
