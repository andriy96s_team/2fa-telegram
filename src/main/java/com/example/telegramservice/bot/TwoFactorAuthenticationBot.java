package com.example.telegramservice.bot;

import com.example.telegramservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.UUID;

@Component
public class TwoFactorAuthenticationBot extends TelegramLongPollingBot {

    private UserService userService;

    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            if (messageText.equals("/register")) {
                processRegisterRequest(update);
            }
        }
    }

    private void processRegisterRequest(Update update) {
        try {
            long chatId = update.getMessage().getChatId();
            System.out.println(chatId);
            SendMessage message = new SendMessage()
                    .setChatId(chatId);
            if (userService.isUserExists(chatId)) {
                message.setText("Your id for two factor authentication: " + userService.getTgIdForExistingUser(chatId));
            } else {
                UUID tgId = UUID.randomUUID();
                userService.saveNewUser(chatId, tgId);
                message.setText("Your id for two factor authentication: " + tgId.toString());
            }
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public boolean sendCode(long chatId, String code) {
        SendMessage message = new SendMessage()
                .setChatId(chatId)
                .setText("Your code is " + code);
        try {
            execute(message);
            return true;
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getBotUsername() {
        return "Two Factor Authentication Bot";
    }

    public String getBotToken() {
        return "1111745371:AAGhArjpoyAy6OEzh4N6P11O3uh7f83BcYk";
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
