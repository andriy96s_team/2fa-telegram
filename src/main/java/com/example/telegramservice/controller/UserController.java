package com.example.telegramservice.controller;

import com.example.telegramservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "http://localhost:8080, https://backend-2fa.herokuapp.com/")
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/sendcode/{tgId}")
    public ResponseEntity<String> sendCode(@PathVariable String tgId) {
        System.out.println("Received request " + tgId);
        String code = userService.sendCode(tgId);
        return new ResponseEntity<>(code, code == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
    }

    @GetMapping("verify/{tgId}/{code}")
    public ResponseEntity verifyCode(@PathVariable String tgId, @PathVariable String code) {
        boolean verified = userService.verifyCode(tgId, code);
        return new ResponseEntity(verified ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
