package com.example.telegramservice.repository;

import com.example.telegramservice.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    public User findByTgId(String tgId);
}
